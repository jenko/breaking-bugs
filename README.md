Breaking Bugs - Unified Diff June 2013
======================================

Installation
-------------

1. Install [Node.js](http://nodejs.org/)

2. Install [Grunt](http://gruntjs.com/getting-started#installing-the-cli)

3. `git clone git@bitbucket.org:jenko/breaking-bugs.git'

4. `sudo npm install`

5. `grunt serve`

6. open http://localhost:8000
